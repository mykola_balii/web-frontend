import axios from "axios";
import env from "react-dotenv";

const endpoint = 'groups'
const url = env.APP_API_URL + endpoint

export async function fetchGroups() {
  const { data } = await axios.get(url)
  return data
}

export async function getGroup(id) {
  const { data } = await axios.get(`${url}/${id}`)
  return data
}