import axios from "axios";
import env from "react-dotenv";

const endpoint = 'students'
const url = env.APP_API_URL + endpoint

export async function fetchStudents() {
  const { data } = await axios.get(url)
  return data.map(item => ({...item, isOnline: false, checked: false}))
}

export async function getStudent(id) {
  const { data } = await axios.get(`${url}/${id}`)
  return data
}

export async function craeteStudent(payload) {
  const { data } = await axios.post(url, payload)
  return data
}

export async function updateStudent(id, payload) {
  const { data } = await axios.put(`${url}/${id}`, payload)
  return data
}

export async function deleteStudents(ids) {
  const { data } = await axios.delete(url, {
    data: {ids}
  })
  return data
}