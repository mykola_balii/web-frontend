import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query'
import React, { useEffect, useState } from 'react'
import { BiPlus } from 'react-icons/bi'
import { toast } from 'react-toastify'
import { fetchGroups } from '../../api/groups'
import { craeteStudent, deleteStudents, fetchStudents, updateStudent } from '../../api/students'
import AddPopup from '../../components/AddPopup/AddPopup'
import ConfirmPopup from '../../components/ConfirmPopup/ConfirmPopup'
import Pagination from '../../components/Pagination/Pagination'
import StudentsTable from '../../components/StudentsTable/StudentsTable'
import Button from '../../components/atoms/Button/Button'
import withLayout from '../../layouts/withLayout'
import styles from './Students.module.css'

const Students = () => {

  const queryClient = useQueryClient()
  const { data: studentsData, isLoading, isError, error } = useQuery({queryKey: ['students'], queryFn: fetchStudents})
  const {data: groupsData, isError: isGroupsError, error: groupsError} = useQuery({queryKey: ['groups'], queryFn: fetchGroups})
  const createStudentMutation = useMutation((newStudent) => craeteStudent(newStudent), {
    onSuccess: (data) => {
      queryClient.invalidateQueries({queryKey: ['students']})
      toast.success(`Successfully added student ${data.first_name} ${data.last_name}`)
    },
    onError: (data) => {
      toast.error(data.response.data.message)
    }
  })
  const updateStudentMutation = useMutation(({id, editedStudent}) => updateStudent(id, editedStudent), {
    onSuccess: (data) => {
      queryClient.invalidateQueries({queryKey: ['students']})
      toast.success(`Successfully updated student ${data.first_name} ${data.last_name}`)
    },
    onError: (data) => {
      toast.error(data.response.data.message)
    }
  })
  const removeStudentsMutation = useMutation((ids) => deleteStudents(ids), {
    onSuccess: (data) => {
      queryClient.invalidateQueries({queryKey: ['students']})
      toast.success(data.message)
    },
    onError: (data) => {
      toast.error(data.message)
    }
  })

  useEffect(() => {
    studentsData && setStudents(studentsData)
  }, [studentsData])

  const [showConfirm, setShowConfirm] = useState(false)
  const [isAddPopupOpen, setIsAddPopupOpen] = useState()
  const [students, setStudents] = useState([])
  const [defaultStudentToEdit, setDefaultStudentToEdit] = useState(null)
  const [isEditPopupOpen, setIsEditPopupOpen] = useState(false)
  const [idsToRemove, setIdsToRemove] = useState([])
  const [userToDelete, setUserToDelete] = useState('')

  useEffect(() => {
    isError && toast.error(error?.message)
    groupsError && toast.error(groupsError?.message)
  }, [isError, error, isGroupsError, groupsError])

  const deletionConfirmed = () => {
    removeStudentsMutation.mutate(idsToRemove)
  }

  const deletionCanceled = () => {
    setShowConfirm(false)
  }

  const removeStudents = (ids) => {
    setIdsToRemove(ids)
    if(ids.length === 1) {
      const student = students.find(item => item.id === ids[0])
      setUserToDelete('user ' + student.first_name + " " + student.last_name)
    } else {
      setUserToDelete(ids.length + ' users')
    }
    setShowConfirm(true)
  }

  const editStudentPopupOpen = (id) => {
    const student = students.find(item => item.id === id)
    if(student) {
      setDefaultStudentToEdit(student)
      setIsEditPopupOpen(true)
    }
  }

  const editStudent = ({id, student}) => {
    updateStudentMutation.mutate({
      id,
      editedStudent: student
    })
  }

  const onSelect = (id, state) => {
    if(id === 'all') {
      setStudents(prevState => {
        return prevState.map(item => ({...item, selected: state}))
      })
    } else {
      setStudents(prevState => {
        return prevState.map(item => {
          if(item.id === id) {
            return {...item, selected: state}
          }
          return item
        })
      })
    }
  }

  const addStudent = (student) => {
    createStudentMutation.mutate(student)
  }

  const onOnline = (id, status) => {
    setStudents(prevState => {
      return prevState.map(item => {
        if(item.id === id) {
          return {...item, isOnline: status}
        }
        return item
      })
    })
  }

  return (
    <div className={styles.students}>
      <div className={styles.students__title}>
        <h2>
            Students
        </h2>
      </div>
      <div className={styles.students__table}>
        <div className={styles.students__table__add}>
          <Button className={styles.students__table__addBtn} onClick={() => setIsAddPopupOpen(true)}>
            <BiPlus size={20} />
          </Button>
        </div>
        <div className={styles.students__table__content}>
          {!isLoading && <StudentsTable 
            students={students} 
            onEdit={editStudentPopupOpen}
            onRemove={removeStudents}
            onSelect={onSelect}
            onOnline={onOnline}
          />}
        </div>
      </div>
      <div className={styles.students__pagination}>
        <Pagination />
      </div>
      {showConfirm && 
        <ConfirmPopup 
          onClose={deletionCanceled} 
          onConfirm={deletionConfirmed} 
          text={`Are you sure you want to delete ${userToDelete}?`} 
        />
      }
      {isAddPopupOpen && <AddPopup onCreate={addStudent} groups={groupsData} onClose={() => setIsAddPopupOpen(false)} />}
      {isEditPopupOpen && 
        <AddPopup 
          onEdit={editStudent} 
          groups={groupsData} 
          defaultStudent={defaultStudentToEdit} 
          onClose={() => setIsEditPopupOpen(false)}  
        />}
    </div>
  )
}

export default withLayout(Students)