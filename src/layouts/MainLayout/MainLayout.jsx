import React, { useState } from 'react'
import Header from '../../components/partials/Header/Header'
import Menu from '../../components/partials/Menu/Menu'
import styles from './MainLayout.module.css'

const MainLayout = ({children}) => {

  const [isMobileMenuOpen, setIsMobileMenuOpen] = useState(false)

  return (
    <>
    <Header toggleMobileMenu={setIsMobileMenuOpen} isMobileMenuOpen={isMobileMenuOpen} />
    <div className={styles.content}>
      <Menu isMobileMenuOpen={isMobileMenuOpen} />
      <main id={styles.main}>
        {children}
      </main>
    </div>
  </>
  )
}

export default MainLayout