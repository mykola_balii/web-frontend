import cn from 'classnames'
import React, { useState } from 'react'
import { BsBell } from 'react-icons/bs'
import { RxHamburgerMenu } from 'react-icons/rx'
import NotificationsDropdown from '../../NotificationsDropdown/NotificationsDropdown'
import ProfileDropdown from '../../ProfileDropdown/ProfileDropdown'
import animations from './BellAnimation.module.css'
import styles from './Header.module.css'

const Header = ({toggleMobileMenu, isMobileMenuOpen}) => {

  const [isNewMessages, setIsNewMessages] = useState(false)
  const [isMessagesRead, setIsMessagesRead] = useState(false)
  const [showMessagesWidget, setShowMessagesWidget] = useState(false)
  const [showProfileWidget, setShowProfileWidget] = useState(false)

  const animate = () => {
    setIsNewMessages(true)
    
    setTimeout(() => {
      setIsNewMessages(false)
    }, 300)
  }

  return (
    <header className={styles.header} style={{position: isMobileMenuOpen && 'fixed'}}>
      <div className={styles.header__logo}>
        <h1 className={styles.header__logoText}>CMS</h1>
        <div className={styles.header__logo__mobileBurger} >
          <RxHamburgerMenu size={25} onClick={() => toggleMobileMenu(!isMobileMenuOpen)} className={styles.header__logo__mobileBurger__icon} />
        </div>
      </div>
      <div className={styles.header__info}>
        <div 
          className={styles.header__notifications} 
          onClick={animate} 
          onDoubleClick={() => setIsMessagesRead(!isMessagesRead)}
          onMouseEnter={() => setShowMessagesWidget(true)}
          onMouseLeave={() => setShowMessagesWidget(false)}
        >
          {!isMessagesRead && <div className={styles.header__notificationsBadge}></div>}
          <BsBell className={cn(styles.header__notificationsIcon, isNewMessages && animations.bellAnimation)} />
          {showMessagesWidget && <NotificationsDropdown className={styles.header__notificationsWidget} />}
        </div>
        <div className={styles.header__profile}
                    onMouseEnter={() => setShowProfileWidget(true)}
                    onMouseLeave={() => setShowProfileWidget(false)}
        >
          <img className={styles.header__profileImage} src="/assets/images/thumbnail.jpeg" alt="avatar" />
          <span className={styles.header__profileName}>James Bond</span>
          {showProfileWidget && <ProfileDropdown className={styles.header__profileWidget} />}
        </div>
      </div>
    </header>
  )
}

export default Header