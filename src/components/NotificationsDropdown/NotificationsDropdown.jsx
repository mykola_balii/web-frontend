import cn from 'classnames'
import React from 'react'
import Notification from '../Notification/Notification'
import styles from './NotificationsDropdown.module.css'

const NotificationsDropdown = ({className}) => {
  return (
    <div className={cn(styles.widget, className)}>
    <div className={styles.widget__content}>
      <ul className={styles.widget__contentList}>
        <li className={styles.widget__contentListItem}>
          <Notification author={'Admin'} className={styles.notification} />
        </li>
        <li className={styles.widget__contentListItem}>
          <Notification author={'John K.'} className={styles.notification} />
        </li>
        <li className={styles.widget__contentListItem}>
          <Notification author={'Marry J.'} className={styles.notification} /> 
        </li>
      </ul>
    </div>
  </div>
  )
}

export default NotificationsDropdown