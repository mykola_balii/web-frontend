import cn from 'classnames'
import React from 'react'
import styles from './ProfileDropdown.module.css'

const ProfileDropdown = ({className}) => {
  return (
    <div className={cn(styles.widget, className)}>
    <div className={styles.widget__content}>
      <ul className={styles.widget__contentList}>
        <li className={styles.widget__contentListItem}>
          Profile
        </li>
        <li className={styles.widget__contentListItem}>
          Log Out
        </li>
      </ul>
    </div>
  </div>
  )
}

export default ProfileDropdown