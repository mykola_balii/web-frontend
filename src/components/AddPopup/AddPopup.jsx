import { useFormik } from 'formik'
import React from 'react'
import { RxCross2 } from 'react-icons/rx'
import * as Yup from "yup"
import Button from '../atoms/Button/Button'
import DateInput from '../atoms/DateInput/DateInput'
import PlainInput from '../atoms/PlainInput/PlainInput'
import SelectInput from '../atoms/SelectInput/SelectInput'
import styles from './AddPopup.module.css'

const genders = [
  {id: 'M', title: 'Male'},
  {id: 'F', title: 'Female'}
]

const userFormSchema = Yup.object().shape({
  group: Yup.string().required("Group is required"),
  firstName: Yup.string().trim()
        .min(2, "Enter more than 1 characters")
        .matches(/^[a-zA-Z ]*$/, "First name should be of type text")
        .required("First name is required"),
  lastName: Yup.string().trim()
        .min(2, "Enter more than 1 characters")
        .matches(/^[a-zA-Z ]*$/, "Last name should be of type text")
        .required("Last name is required"),
  gender: Yup.string().required("Gender is required"),
  date: Yup.date().required("Date is required")
})

const AddPopup = ({groups = [], defaultStudent = null, onCreate, onEdit = undefined, onClose}) => {

  const mapFormFieldsAndCreateUserObject = (formData) => {
    return {
      first_name: formData.firstName,
      last_name: formData.lastName,
      gender: formData.gender,
      birthday: formData.date,
      group_id: formData.group,
    }
  }

    
  const close =() => {
    onClose()
  }

  const create = (formData) => {
    onCreate(mapFormFieldsAndCreateUserObject(formData))
    close()
  }

  const edit = (formData) => {
    onEdit({id: defaultStudent.id, student: mapFormFieldsAndCreateUserObject(formData)})
    close()
  }

  const studentForm = useFormik({
    initialValues: {
      group: defaultStudent ? defaultStudent.group_id : "",
      firstName: defaultStudent ? defaultStudent.first_name : "",
      lastName: defaultStudent ? defaultStudent.last_name : "",
      gender: defaultStudent ? defaultStudent.gender : "",
      date: defaultStudent ? defaultStudent.birthday : ""
    },
    validationSchema: userFormSchema,
    onSubmit: (values) => {
      if(defaultStudent) {
        edit(values)
      } else {
        create(values)
      }
    }
  })

  return (
    <div className={styles.backdrop}>
      <div className={styles.popup}>
        <div className={styles.popup__header}>
          <h3 className={styles.popup__headerTitle}>{defaultStudent ? 'Edit student' : 'Add student'}</h3>
          <Button className={styles.popup__headerClose} outlineThin onClick={close}>
            <RxCross2 size={20} />
          </Button>
        </div>
        <div className={styles.popup__content}>
          <form className={styles.popup__contentForm} onSubmit={studentForm.handleSubmit} >
          <div className={styles.popup__contentFormGroup}>
              Group <SelectInput 
                      options={groups} 
                      onChange={studentForm.handleChange} 
                      value={studentForm.values.group} 
                      className={styles.popup__contentFormInput}
                      error={(studentForm.touched.group && studentForm.errors.group) ? studentForm.errors.group : null}
                      placeholder={'Select group'}
                      {...studentForm.getFieldProps('group')}
                    />
            </div>
            <div className={styles.popup__contentFormGroup} >
              First name <PlainInput 
                            onChange={studentForm.handleChange} 
                            value={studentForm.values.firstName} 
                            className={styles.popup__contentFormInput}
                            error={(studentForm.touched.firstName && studentForm.errors.firstName) ? studentForm.errors.firstName : null}
                            {...studentForm.getFieldProps('firstName')}
                          />
            </div>
            <div className={styles.popup__contentFormGroup} >
              Last name <PlainInput 
                            onChange={studentForm.handleChange} 
                            value={studentForm.values.lastName} 
                            className={styles.popup__contentFormInput}
                            error={(studentForm.touched.lastName && studentForm.errors.lastName) ? studentForm.errors.lastName : null}
                            {...studentForm.getFieldProps('lastName')}
                        />
            </div>
            <div className={styles.popup__contentFormGroup}>
              Gender <SelectInput 
                        options={genders} 
                        onChange={studentForm.handleChange} 
                        value={studentForm.values.gender} 
                        className={styles.popup__contentFormInput}
                        error={(studentForm.touched.gender && studentForm.errors.gender) ? studentForm.errors.gender : null} 
                        placeholder={'Select gender'}
                        {...studentForm.getFieldProps('gender')}
                      />
            </div>
            <div className={styles.popup__contentFormGroup} >
              Birthday <DateInput 
                        onChange={studentForm.handleChange} 
                        value={studentForm.values.date} 
                        className={styles.popup__contentFormInput}
                        error={(studentForm.touched.date && studentForm.errors.date) ? studentForm.errors.date : null}
                        {...studentForm.getFieldProps('date')}
                      />
            </div>
          </form>
        </div>
        <div className={styles.popup__footer}>
          <div className={styles.popup__footerButtons}>
            <Button text={'Ok'} outlineThin className={styles.popup__footerButtonsItem} onClick={close} />
            {!defaultStudent && <Button text={'Create'} type="submit" outlineThin className={styles.popup__footerButtonsItem} onClick={studentForm.handleSubmit} />}
            {defaultStudent && <Button text={'Save'} type="submit" outlineThin className={styles.popup__footerButtonsItem} onClick={studentForm.handleSubmit} />}
          </div>
        </div> 
      </div>
    </div>
  )
}

export default AddPopup