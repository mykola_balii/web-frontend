import cn from 'classnames'
import React from 'react'
import styles from './DateInput.module.css'

const DateInput = ({className, error, ...props}) => {

  return (
    <div className={styles.wrapper}>
        <input type='date' className={cn(styles.input, error && styles.inputError, props.value === "" && styles.placeholder, className)} min={'1970-01-01'} {...props}/>
        {error && <span className={styles.error}>{error}</span>}
    </div>
  )
}

export default DateInput