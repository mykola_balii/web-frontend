import cn from 'classnames'
import React from 'react'
import styles from './PlainInput.module.css'

const PlainInput = ({className, error, ...props}) => {

  return (
    <div className={styles.wrapper}>
      <input type='text' className={cn(styles.input, error && styles.inputError, className)} {...props}/>
      {error && <span className={styles.error}>{error}</span>}
    </div>
  )
}

export default PlainInput