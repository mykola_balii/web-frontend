import cn from 'classnames'
import React, { useEffect } from 'react'
import { MdArrowDropDown } from 'react-icons/md'
import styles from './SelectInput.module.css'

const SelectInput = ({className, options = [], error, ...props}) => {

  useEffect(() => {
    console.log(options);
  }, [options])
  return (
    <div className={cn(styles.wrapper, className)}>
      <select 
        className={cn(styles.select, error && styles.inputError, props.value === "" && styles.placeholder)} 
        {...props}
      >
      <option disabled value="" className={styles.placeholder}>{props.placeholder}</option>
        {options && options.map(item => {
          return <option value={item.id} key={item.id} >{item.title}</option>
        })}
      </select>
      <MdArrowDropDown size={40} className={styles.arrow} />
      {error && <span className={styles.error}>{error}</span>}
    </div>
  )
}

export default SelectInput